# OSGi Configuration Admin command line client - Releases

## Version 0.4 (september 2013)

* added new "clear" command, to remove a key/value from a configuration
* small fixes w.r.t. formatting of help text on Equinox platform


## Version 0.3.1 (january 2013)

* moved to [Gradle](http://www.gradle.org/) for building
* added documentation as Markdown file
* no functional changes w.r.t. version 0.3